function ckEditor(element) {
  return CKEDITOR.replace(element, {
    removePlugins: 'elementspath',
    resize_enabled: false,
    toolbar: [
      {name: 'document', items: ['NewPage', 'Preview', '-', 'Templates']},    // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
      ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],            // Defines toolbar group without name.
      '/',                                                                                    // Line break - next group will be placed in new line.
      {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat']
      },
      // { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      {
        name: 'paragraph',
        groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
      },
      // { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    ]
  });
}


