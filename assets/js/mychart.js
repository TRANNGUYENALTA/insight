function drawChart1(ip, isSearch,arrLocation) {
  console.log(isSearch);
  $(document).ready(function () {
    // Trang mac dinh
    DashbroadLocation();

    $('.fa-sync-alt').on('click', function () {
      $('#line-tab .fa-eye').toggleClass("animation__eye");
      DashbroadLocation();
    })
    setFunction();
  });
  if (isSearch) {
    (function () {
      [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el);
      });
    })();
  }

  $(document).on('click', '.dropdate', function () {
    $("#datepicker3").datepicker('show');
  })

  function show_chart() {
    $.ajax({
      url: ip + '/api/customer/live_hostpot',
      type: 'post',
      data: {
        "location": arrLocation
      },
      success: function (dt) {
        Highcharts.chart('container1', {
          chart: {
            type: 'spline',
            scrollablePlotArea: {
              Width: 600,
              scrollPositionX: 1
            }
          },
          title: {
            text: ' '
          },

          xAxis: {
            type: 'datetime',
            lineColor: 'lightgray',
            labels: {
              overflow: 'justify'
            },
          },
          yAxis: {
            title: {
              text: ' '
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
          },

          plotOptions: {
            spline: {
              lineWidth: 4,
              states: {
                hover: {
                  lineWidth: 5
                }
              },
              marker: {
                enabled: false
              },
              pointInterval: 300000, // one hour
              pointStart: dt.second
            }
          },
          series: [{
            name: dt.data[0].name,
            color: '#F05A29',
            lineColor: '#F05A29',
            labels: {
              enabled: false
            },
            marker: {
              fillColor: '#F05A29', // dau tron
              lineWidth: 2,
              lineColor: '#F05A29'
            },
            data: dt.data[0].data
          }],
          navigation: {
            menuItemStyle: {
              fontSize: '10px'
            }
          }
        });

      }
    })
    ;
  };

  function show_chart2() {
    $.ajax({
      url: ip + '/api/customer/live_detect',
      type: 'post',
      data:
        {
          "location": arrLocation
        },
      success: function (dt) {
        Highcharts.chart('container2', {
          chart: {
            type: 'spline',
            scrollablePlotArea: {
              Width: 600,
              scrollPositionX: 1
            }
          },
          title: {
            text: ' '
          },
          xAxis: {
            type: 'datetime',
            lineColor: 'lightgray',
            labels: {
              overflow: 'justify'
            },
          },
          yAxis: {
            title: {
              text: ' '
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
          },

          plotOptions: {
            spline: {
              lineWidth: 4,
              states: {
                hover: {
                  lineWidth: 5
                }
              },
              marker: {
                enabled: false
              },
              pointInterval: 300000,
              pointStart: dt.second
            }
          },
          series: [{
            name: dt.data[0].name,
            color: '#F05A29',
            lineColor: '#F05A29',
            labels: {
              enabled: false
            },
            marker: {
              fillColor: '#F05A29',
              lineWidth: 2,
              lineColor: '#F05A29'
            },
            data: dt.data[0].data
          }],
          navigation: {
            menuItemStyle: {
              fontSize: '10px'
            }
          }
        });

      }
    })
    ;
  };

  function setFunction() {
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    today = dd + '-' + mm + '-' + yyyy;
    document.getElementById('datepicker3').value = today;
  }

  function show_chart3(dateForChart) {
    console.log(dateForChart)
    $.ajax({
      url: ip + '/api/customer/chart1',
      type:
        'post',
      data:
        {
          "location": arrLocation,
          "date": dateForChart
        },
      success: function (dt) {
        Highcharts.chart('container3', {
          chart: {
            type: 'column'
          },
          title: {
            text: ' '
          },
          subtitle: {
            text: ' '
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: ' '
            }
          },
          legend: {
            enabled: false
          },
          plotOptions: {
            series: {
              borderWidth: 0,
              dataLabels: {
                enabled: true,
                format: '{point.y:.1f}'
              }
            }
          },
          tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
          },
          "series": [
            {
              "name": "Browsers",
              "colorByPoint": true,
              "data": dt.data
            }
          ],

        });
      }
    });
  };

  var myTable;

  function TableLocation() {
    var a = document.getElementById('datepicker3').value;
    // console.log(a);
    myTable = $('#dashboardtable').DataTable({
      "ordering": false,
      "bLengthChange": false,
      "searching": false,
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      language: {
        paginate: {
          previous: "<a class='pr-2'><i class='fa fa-caret-left'></i></a>",
          next: "<a class='pl-2'><i class='fa fa-caret-right'></i></a>"
        }
      },
      ajax: {
        url: ip + '/api/customer/live_detail/',
        type: "post",
        data: {
          "location": arrLocation
        }
      },
      columns:
        [
          {data: 'name'},
          {data: 'mac_address'},
          {data: 'location'},
          {data: 'time'},
          {data: 'device_name'},
        ],
      columnDefs:
        [
          {
            'targets': 5,
            'render': function (data, type, row1) {

              if (row1.status == "1") {
                return '<span><i class="fas fa-wifi"></i></span>';
              }
              else if (row1.status == "2") {
                return "<span><img style='width:17px;height:17px;' src='http://192.168.11.201/insight/public/img/radar-512.png'></span>";
              }
            }
          }
        ],
      "initComplete": function (settings, json) {
        var info = myTable.page.info();
        $('#page_table').text('Page ' + (info.page + 1) + ' of ' + info.pages)
      },
    });
  }

  function DashbroadLocation() {
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' +dd ;

    var location = $.ajax({
      url: ip + '/api/location',
      type: 'GET',
      success: function (data) {
        var color;
        $('#favorite_DB').select2({
          placeholder: "Choose location",
          allowClear: true,
          templateSelection: function (dt, container) {
            $.each(data.data, function (key, value) {
              switch (dt.id) {
                case value.id:
                  color = value.color;
                  break;
              }
            });
            $(container).css("background-color", color);
            return dt.text;
          }
        });
        var html = '';
        var arr = [];
        $.each(data.data, function (key, value) {
          html += "<option value='" + value.id + "'>" + value.description + "</option>";
          arr.push(value.color);
          arrLocation.push(value.id);
        });
        show_chart();
        show_chart2();
        show_chart3(today);
        TableLocation();
        $("#favorite_DB").html(html);
      },
    });
  }

  var date_chart1 = '';
  $("#datepicker3").datepicker({
    autoclose: true,
    format: "dd-mm-yyyy"
  }).on("change", function () {
    var day = $(this).datepicker('getDate').getDate();
    var month = $(this).datepicker('getDate').getMonth() + 1;
    var year = $(this).datepicker('getDate').getFullYear();

    if (month < 10) {
      month = '0' + month
    }
    if (day < 10) {
      day = '0' + day
    }
    date_chart1 = year + '-' + month + '-' + day;
    date_tampchart1 = month + '/' + day + '/' + year;
    date_temp = date_chart1;
    document.getElementById('date__overview').value = date_chart1;
    show_chart3(date_chart1)
  });

  $('#favorite_DB').change(function (e) {
    date_chart1 = document.getElementById('date__overview').value
    arrLocation = [];
    var selections = ($('#favorite_DB').select2('data'));
    $.each(selections, function (i, v) {
      arrLocation.push(v.id);
    })
    if (!arrLocation.length > 0) {
      DashbroadLocation();
    }
    else {
      show_chart();
      show_chart2();
      show_chart3(date_chart1)
      TableLocation();
    }
  });

  $('#table__next').click(function () {
    $('.paginate_button.next').click();
    var info = myTable.page.info();
    $('#page_table').text('Page ' + (info.page + 1) + ' of ' + info.pages)
  });
  $('#table__prev').click(function () {
    $('.paginate_button.previous').click()
    var info = myTable.page.info();
    $('#page_table').text('Page ' + (info.page + 1) + ' of ' + info.pages)
  });
}
