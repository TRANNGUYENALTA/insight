var zoomIn = true;
var datas = '';
var isToggle = false;

var isImageFeed = false;
var isImageLogin = false;
var isImageLanding = false;
var isImageLoginDesk = false;
var isImageLandingDesk = false;
$(document).ready(function () {
  // delete image error
  // refresh page
  $(document).on('click', '#resfeshDashBoard', function () {
    $('#icon-search').click();
  });
  // active gradient
  activeGradientChoosed('.gradient_landing');
  activeGradientChoosed('.gradient_login_template');
  activeGradientChoosed('.gradient_item_login_desk');
  activeGradientChoosed('.gradient_landing_desk');
  // $(document).on('click', '.gradient_landing', function () {
  //   $('.gradient_landing').removeClass('active');
  //   $(this).addClass('active');
  // });
  //
  // $(document).on('click', '.gradient_login_template', function () {
  //   $('.gradient_login_template').removeClass('active');
  //   $(this).addClass('active');
  // });


  // end active gradient
  $('div[contenteditable]').keydown(function (e) {
    // trap the return key being pressed
    if (e.keyCode === 13) {
      // insert 2 br tags (if only one br tag is inserted the cursor won't go to the next line)
      document.execCommand('insertHTML', false, '<br><br>');
      // prevent the default behaviour of return key pressed
      return false;
    }
  });

  $('#multi-select').dropdown();
  //click icon search
  // Dragable
  $('#map-office').draggable();
  $('.avatar-office').draggable();
  // click map office zoomin

  //set width progress bar
  setWidthProgressBar();


  // set color background menu item
  // clickItem()
});

function setNavLeftIcon() {
  setIconActiveNavLink();
  // Set Title Page
  $(document).on('click', '.nav-item', function () {
    var tt = $(this).children('.nav-link').data('title');
    $('#titlePage').text(tt);
  });
  $('.list-menu .nav-item').on('click', function () {
    $('.list-menu .nav-item').removeClass('active');
    $(this).addClass('active');
    setIconNormalNavLink();
    setIconActiveNavLink();
    setMouseoverMenuNav();
    setMouseOutMenuNav();
    var srcActive = $(this).children('.nav-link').children('.icon-nav').data('link-active');
    $(this).children('.nav-link').children('.icon-nav').attr('src', srcActive);
  })
  setMouseoverMenuNav();
  setMouseOutMenuNav();
}

function clickItem() {
  $(document).on('click', '.menu .item', function () {
    set();
  })
}

function setMouseoverMenuNav() {
  $('.list-menu .nav-item').not('.active').children('.nav-link').mouseover(function () {
    var src = $(this).children('.icon-nav').data('link-active');
    $(this).children('.icon-nav').attr('src', src);
    $('.list-menu .nav-item .icon-nav').css({'width': '25px!important', 'height': 'auto!important'});
    setIconActiveNavLink();
  });
}

function setMouseOutMenuNav() {
  $('.list-menu .nav-item').not('.active').children('.nav-link').mouseout(function () {
    var src = $(this).children('.icon-nav').data('link-normal');
    $(this).children('.icon-nav').attr('src', src);
    $('.list-menu .nav-item .icon-nav').css({'width': '25px!important', 'height': 'auto!important'});
    setIconActiveNavLink();
  });
}

function setIconActiveNavLink() {
  $('.list-menu .nav-item.active').each(function () {
    var src = $(this).children('.nav-link').children('.icon-nav').data('link-active');
    $(this).children('.nav-link').children('.icon-nav').attr('src', src);
  })
}

function setIconNormalNavLink() {
  $('.list-menu .nav-item').each(function () {
    var src = $(this).children('.nav-link').children('.icon-nav').data('link-normal');
    $(this).children('.nav-link').children('.icon-nav').attr('src', src);
  })
}

// set icon profile color
function setColorProfile(obj, css) {
  $(obj).each(function () {
    var type = $(this).data('profile');
    switch (type) {
      case 'audit': {
        $(this).css(css, auditColor);
        break;
      }
      case 'tax': {
        $(this).css(css, taxColor);
        break;
      }
      case 'advisory': {
        $(this).css(css, advisoryColor);
        break;
      }
      case 'centralsrv': {
        $(this).css(css, centralsrvColor);
        break;
      }
    }
  });
}


function set() {
  setTimeout(function () {
    setBackgroundProfile();
  }, 50);
}

// set background profile
function setBackgroundProfile() {
  $('.label').each(function () {
    for (i = 0; i < datas.length; i++) {
      if ($(this).data('value') == datas[i].id) {
        $(this).css('background-color', datas[i].color)
      }
    }
  });
}

function closeModal() {
  $('.close').click();
}

// map
function mapAction() {
  $('#map-office').draggable();
  $('.avatar-office').draggable();
}

function setWidthProgressBar() {
  var father = $('.each-favorite').each(function () {
    var perCent = $(this).children('.progress-bar').data('percent');
    $(this).children('.progress-bar').css('width', perCent);
    $(this).children('.number-percent').text(' ' + perCent + ' %');
  });
}

function setHeightElement(ele) {
  // set height for tab-content
  var height = $(window).height() - $(ele).offset().top - 20;
  $(ele).css('height', height);
}

function zoomin() {

  // zoomIn = true;
  // var myImg = $("#myCanvas");
  // var curWidth = myImg.width();
  // var curHeight = myImg.height();
  // if (curWidth === 2500) return false;
  // else {
  //   let width = curWidth + curWidth * 0.2;
  //   let height = width * curHeight / curWidth;
  //   myImg.css("width", width);
  //   myImg.css("height", height);
  // }


}

function zoomout() {
  zoomIn = false;
  var myImg = $("#myCanvas");
  var curWidth = myImg.width();
  var curHeight = myImg.height();
  if (curWidth === 100) return false;
  else {
    let width = curWidth - curWidth * 0.2;
    let height = width * curHeight / curWidth;
    myImg.css("width", width);
    myImg.css("height", height);
  }
}

//side bar
function openNav(ele) {
  $(ele).css('right', '0');
}

function closeNav(ele) {
  $(ele).css('right', '-450px');
}

function openChatBox(ele) {
  $(ele).css('right', '45px');
}

function closeChatBox(ele) {
  $(ele).css('right', '-410px');
}

//close form create
function closeFormCreate() {
  $('#close-create-rule').click();
}

function closeModalMail() {
  $('#modalbox-email-header-current .close').click();

}

function toggleMenu() {
  isToggle = !isToggle;
  if (isToggle) {
    $('#menuLeft').addClass('toggle-menu');
    $('#content-right').addClass('toggle-menu');
    $('#menuLeft_responsive').addClass('toggle-menu');
  } else {
    $('#menuLeft').removeClass('toggle-menu');
    $('#content-right').removeClass('toggle-menu');
    $('#menuLeft_responsive').removeClass('toggle-menu');
  }
}

// i =1 check for login template
// i = 2 check for landing template
function checkImageTemplate(obj, i,w , h) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        console.log(1);
        if (this.width >= w && this.height >= h) {
          setVarIsCheckImage(i, true);
        } else {
          setVarIsCheckImage(i, false);
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      setVarIsCheckImage(i, true);
    }
  });
}

function checkImageFeed(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 256 && this.height == 144) {
          isImageFeed = true;
        } else {
          isImageFeed = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImageFeed = true;
    }
  });
}

function setVarIsCheckImage(i, value) {
  switch (i) {
    case 1: {
      isImageLogin = value;
      break;
    }
   case 2: {
      isImageLanding = value;
      break;
    }
   case 3: {
      isImageLoginDesk = value;
      break;
    }
   case 4: {
     isImageLandingDesk = value;
      break;
    }
  }
}

// template 02
function readURLFileTemplate(type, input, objImg, objTitle) {
  var isTrue = false;
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        switch (type) {
          case 1: {
            isTrue = isImageLogin;
            break;
          }
          case 2: {
            isTrue = isImageLanding;
            break;
          }
          case 3: {
            isTrue = isImageLoginDesk;
            break;
          }
          case 4: {
            isTrue = isImageLandingDesk;
            break;
          }
        }
        console.log(2);
        if (isTrue) {
          $(objImg).attr('src', e.target.result);
          $(objImg).css('display', 'inline-block');
          $(objTitle).text(input.files[0].name);
          $(objTitle).attr('title', input.files[0].name);
        } else {
          $(objImg).attr('src', '');
          $(objTitle).text("Upload Image");
          $(objTitle).attr('title', "Upload Image");
        }

      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      $(objImg).attr('src', '');
      $(objTitle).text("Upload Image");
      $(objTitle).attr('title', "Upload Image");
    }
  }, 100)
}

// arrImg.push($('.item').length);
function readURL(input, bla) {
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        if (isImageFeed) {
          $(bla)
            .attr('src', e.target.result)
            .width(270)
            .height(180);
          $(bla + '_plus_img').css('display', 'none');
          $('#plus_imgex').css('display', 'none!important');
          $('#btn_addimg').css('display', 'none');
          $(bla + '_add_img').css('display', 'none');
        } else {
          $(bla).attr('src', "").removeAttr('style');
          $(bla + '_plus_img').css('display', 'inline-block');
          $('#plus_imgex').css('display', 'inline-block');
          $('#btn_addimg').css('display', 'block');
          $(bla + '_add_img').css('display', 'inline-block');
        }

      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      $(bla).attr('src', "").removeAttr('style');
      $(bla + '_plus_img').css('display', 'inline-block');
      $('#plus_imgex').css('display', 'inline-block');
      $('#btn_addimg').css('display', 'block');
      $(bla + '_add_img').css('display', 'inline-block');
    }
  }, 20)

}

function css() {
  $('.recurrence').css('color', '#c3bfbf').css('background-color', '#dedbdb');
}

function ShowDetail(id) {
  setTimeout(function () {
    $("#ShowDetail").trigger('click')
  }, 1)
  $("#hidId").val(id)

}
// active gradient choose
function activeGradientChoosed(obj) {
  // active gradient
  $(document).on('click', obj, function () {
    $(obj).removeClass('active');
    $(this).addClass('active');
  });

}
