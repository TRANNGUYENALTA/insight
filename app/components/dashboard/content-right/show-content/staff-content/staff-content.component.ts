import {Component, OnInit, OnDestroy, ViewChild, AfterViewInit} from '@angular/core';
import './../../../../../../assets/js/staff.js';
import {StaffService} from '../../../../../services/staff/staff.service';
import {SearchProfileService} from '../../../../../services/searchProfile/search-profile.service';
import {DetailPersonService} from '../../../../../services/detailPerson/detail-person.service';
import {MailService} from '../../../../../services/mail/mail.service';
// Data table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';


declare var ckEditor: any;
declare var datas: any;
declare var $: any;
declare var openNav: any;
declare var closeNav: any;
declare var clickItem: any;

@Component({
  selector: 'app-staff-content',
  templateUrl: './staff-content.component.html',
  styleUrls: ['./staff-content.component.css']
})
export class StaffContentComponent implements OnInit, OnDestroy, AfterViewInit {

  public allFunction: any;
  public functionsSearch: any = [];
  public functionsSearchStr: any = '';
  public title = 'Customers';
  public infor: any = '';
  public logs: any = '';
  // Send Mail
  public email: any = '';
  public description: any = '';
  // is loading staff
  public isLoadingStaff: any = false;
  // Data table
  public currentPage: any = 1;
  public ip: any = '';
  public idPrev: any = null;
  public isShowInfo: any = false;

  constructor(public staffService: StaffService,
              public searchProfileService: SearchProfileService,
              public detailPersonService: DetailPersonService,
              private mailService: MailService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService) {
    this.ip = config.getIpInSight();
  }

  ngOnInit() {
    clickItem();

    // search
    $('#multi-select').dropdown();
    // get data
    this.getAllFunction();
    // Data table
    this.getAllCustomer();
  }

  getInforPersonStaff(id) {
    this.isShowInfo = !this.isShowInfo;
    if (id === this.idPrev) {
      if (this.isShowInfo) {
        openNav('#member-detail');
      } else {
        closeNav('#member-detail');
      }
    } else {
      this.isShowInfo = true;
      this.isLoadingStaff = true;
      this.idPrev = id;
      this.detailPersonService.getInfomation(id).subscribe(res => {
        this.infor = res.data;
        this.isLoadingStaff = false;
      }, err => {
        console.log(err);
        this.isLoadingStaff = false;
      });
    }
  }

  getLogPersonStaff(id) {
    this.destroyDataTable('#logTable');
    this.detailPersonService.getLog(id).subscribe(res => {
      this.logs = res.data;
      console.log(res.data);
      this.chRef.detectChanges();
      $('#logTable').DataTable({
        'pageLength': 1000,
        // 'scrollY': '563px'
      });
    }, error => {
      console.log(error);
    });
  }

  getAllFunction() {
    this.searchProfileService.getFunctions().subscribe(res => {
      this.allFunction = res.data;
      datas = this.allFunction;
    }, error1 => {
      console.log(error1);
    });
  }

  getFunctionSearch(data) {
    this.functionsSearchStr = data;
    console.log(data);
    let arr = data.split(',');
    if (!data) {
      arr = [];
    }
    this.functionsSearch = arr;
    this.getAllCustomer();
  }

  ShowDetail() {
    let ids;
    ids = $('#hidId').val();
    openNav('#member-detail');
    this.getInforPersonStaff(ids);
  }


  getAllCustomer() {
    const that = this;
    this.destroyDataTable('#table');
    $('#table').DataTable({
      processing: true,
      'pageLength': 8,
      'serverSide': true,
      ajax: {
        url: this.ip + '/api/customer_device?location=' + that.functionsSearchStr,
      },
      'drawCallback': function (settings) {
        that.setPagiInfo('#table', '#navigation_customer');
        $(document).on('click', '.sendMail', function () {
          const email = $(this).data('email');
          that.getEmail(email);
        });

      },
      columns: [
        {data: 'name', name: 'name'},
        {data: 'phone', name: 'phone'},
        {data: 'mac_address', name: 'mac_address'},
        {data: 'device_name', name: 'device_name'},
        {data: 'device_type', name: 'device_type'},
      ],
      columnDefs: [
        {
          'targets': 0,
          'render': function (data, type, row) {
            return `<div class="line h-100" style="background-color:` + row.color + ` "></div>` +
              `<div class="rounded-circle dot" style="background-color:` + row.gender + ` "></div>` +
              `<img src="./assets/images/avatar_person.png"class="rounded-circle img customerClick"   onclick="closeNav('#list-profile'),ShowDetail(` + row.id + `)" data-id="` + row.id + `" style="border: 2px solid ` + row.color + `;width: 45px; height: 45px;" onclick="closeNav('#list-profile')" style="cursor: pointer">` +
              `<div style="display: inline-block; margin-left: 10px; margin-top: -18px;"  onclick="closeNav('#list-profile'),ShowDetail(` + row.id + `)" class="customerClick" data-id="` + row.id + `">
                    <span>` + row.name + `</span>
                </div>`;
          }
        }
      ]
    });
    this.actionPagiPage('#table', '#navigation_customer');
  }


// get email
  getEmail(_email) {
    console.log(_email);
    this.email = _email;
  }

  ngAfterViewInit() {
  }

// get description
  sendMail(des) {
    $('#modal-mail .close').click();
    this.description = des.value;
    this.mailService.sendMail({
      'email': this.email,
      'description': this.description
    }).subscribe(res => {
      $('#editor1').val('');
      swal('Send Mail Success!', '', 'success');
    }, err => {
      swal('Send Mail Failure!', '', 'error');
    });

  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }


  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    if (table && table.page.info()) {
      const currentPage = table.page.info().page + 1;
      const totalPage = table.page.info().pages;
      const pagi = 'Page ' + currentPage + ' of ' + totalPage;
      $(objPagi + ' .pagin').html(pagi);
    } else {
      $(objPagi + ' .pagin').html('Page 1 of 1');
    }
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }

}
