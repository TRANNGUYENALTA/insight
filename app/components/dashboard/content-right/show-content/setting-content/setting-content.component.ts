import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SettingService} from '../../../../../services/setting/setting.service';
import {User} from '../../../../../models/setting/user';
import swal from 'sweetalert';
import {ChangeDetectorRef} from '@angular/core';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';
import {ActivatedRoute, Router} from '@angular/router';
import {throws} from 'assert';

declare var $: any;

@Component({
  selector: 'app-setting-content',
  templateUrl: './setting-content.component.html',
  styleUrls: ['./setting-content.component.css']
})
export class SettingContentComponent implements OnInit, OnDestroy, AfterViewInit {
  public zones: any = '';
  public users: any = '';
  // zone
  public tags: any = '';
  // Ytu
  public adUser: User = new User();
  public upUser: User = new User();

  public IP: any = '';
  // is show loading table user
  public isShowTableUser = true;

  constructor(public settingService: SettingService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService,
              private router: Router) {
    this.IP = config.getIp();
  }

  ngOnInit() {
    this.setHeigth('#setting-content');
    $('.myTags').select2({
      allowClear: true
    });
    $('#favorite_update').select2({
      allowClear: true,
    });
    this.getUsers();
  }

  setHeigth(ele) {
    const height = $(window).height() - $(ele).offset().top - 20;
    $(ele).css('height', height);
  }

  getDetail(_id) {
    this.router.navigate(['insight/ads', _id]);
  }

  getUsers() {
    this.settingService.getLocation().subscribe(res => {
      this.isShowTableUser = false;
      this.destroyDataTable('#table_user');
      this.users = res.data;
      this.dataTablesConfig('#table_user', '#navigation_user');
    }, err => {
      console.log(err);
    });
  }
  addUser(location, description, color) {
    const newForm = new FormData();
    newForm.append('name', location.value);
    newForm.append('description', description.value);
    newForm.append('color', color.value);
    this.settingService.addUserSetting(newForm).subscribe(res => {
      swal('Add Location Success!', '', 'success');
      this.getUsers();
      location.value = '';
      description.value = '';
      color.value = '';
    }, err => {
      swal(err.error.message, '', 'error');
    });
  }


  updateUser(id?) {
    const array = [];
    $('#list-permission-update input:checkbox:checked').map(function () {
      array.push($(this).val());
    }).get();
    this.upUser.permission = array;
    this.settingService.updateUserSetting(this.upUser).subscribe(res => {
      swal('Update Success!', '', 'success');
      this.getUsers();
      this.resetPassword();
    }, err => {
      swal('Update Failure!', '', 'error');
    });
  }

  resetPassword() {
    this.upUser.password = '';
  }

  deleteUser(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this user information!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          this.settingService.deleteUserSetting(id).subscribe(res => {
            this.getUsers();
            swal('Delete Success!', '', 'success');
          }, err => {
            swal('Not Delete!', '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  getUserById(id) {
    this.settingService.getUSerSettingById(id).subscribe(res => {
      this.assignUpDateUserVarible(res.data);
    }, err => {
      console.log(err);
    });
  }

  assignUpDateUserVarible(data) {
    this.upUser.id = data.Employment.id;
    this.upUser.full_name = data.Employment.full_name;
    this.upUser.phone = data.Employment.phone;
    this.upUser.email = data.Employment.email;
    $('#list-permission-update input:checkbox').map(function () {
      const that = $(this);
      for (let i = 0; i < data.role.length; i++) {
        const valueOfRole = '' + data.role[i];
        if (that.val() === valueOfRole) {
          that.attr('checked', true);
          break;
        }
      }
    });
  }


  ngOnDestroy() {
  }

  ngAfterViewInit() {
  }

  // dataTable function
  dataTablesConfig(objTable, objPagi) {
    // You'll have to wait that changeDetection occurs and projects data into
    // the HTML template, you can ask Angular to that for you ;-)
    this.chRef.detectChanges();
    $(objTable).DataTable({
      'pageLength': 10
    });

    this.setPagiInfo(objTable, objPagi);
    this.actionPagiPage(objTable, objPagi);
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    if (table && table.page.info()) {
      const currentPage = table.page.info().page + 1;
      const totalPage = table.page.info().pages;
      const pagi = 'Page ' + currentPage + ' of ' + totalPage;
      $(objPagi + ' .pagin').html(pagi);
    } else {
      $(objPagi + ' .pagin').html('Page 1 of 1');
    }
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }
}
