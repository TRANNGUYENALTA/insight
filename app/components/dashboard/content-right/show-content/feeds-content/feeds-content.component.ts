import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FeedsService} from '../../../../../services/feeds/feeds.service';
import './../../../../../../assets/js/feed.js';
import {Feed, FeedInfor, RuleFeedInformation} from '../../../../../models/feed/feed';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';
import {RuleService} from '../../../../../services/rules/rule.service';
import {PromoTemplate} from '../../../../../models/feed/feed';
import {SearchProfileService} from '../../../../../services/searchProfile/search-profile.service';
import {ActivatedRoute} from '@angular/router';
// Data Table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';

declare var $: any;
declare var setHeightElement: any;
declare var feedFunction: any;
declare var monthClick: any;
declare var getselect2: any;
declare var setTag: any;
declare var CKEDITOR: any;
// check image
declare var checkImageFeed: any;
declare var isImageFeed: any;
declare var checkImageTemplate: any;
declare var isImageLogin: any;
declare var isImageLanding: any;
declare var isImageLoginDesk: any;
declare var isImageLandingDesk: any;

declare var css: any;
declare var show_icon: any;
declare var show_anypicker: any;
declare var escapeRegExp: any;


declare var removeclass: any;
declare var show_table_emojiAddfeed: any;
// draw canvas
declare var ckEditor: any;

@Component({
  selector: 'app-feeds-content',
  templateUrl: './feeds-content.component.html',
  styleUrls: ['./feed.scss']
})

export class FeedsContentComponent implements OnInit, OnDestroy, AfterViewInit {
  public subcription1: Subscription;
  public subcription2: Subscription;
  public dataMonth: any = '';
  public feed: Feed = new Feed();
  public updateFeed: Feed = new Feed();
  public feedById: any = '';
  public input = new FormData();
  public inputUpdate = new FormData();
  public isUpdate: any = true;
  public idUpdate: any;
  // Config domain
  public IP: any = '';
  // check image
  public isFeedImage: any = true;
  public isLoginImage: any = true;
  public isLandingImage: any = true;
  public isLoginImageDesk: any = true;
  public isLandingImageDesk: any = true;
  // Feed Information Rules
  public condition: any = '';
  public showIcon: any = 'd-none';

  // Loading
  public isLoadingCalendar: any = true;
  // Promo
  public idTemplate: any = '';

  // Templates
  public templates: any = '';
  public templatesDemo: any = '';
  // icon template
  // image template
  public imgTemplateType1: any = '';
  public imgTemplateType2: any = '';
  // disabled div
  public step_02 = 'disableDiv';
  public step_03 = 'disableDiv';
  // check stept 2
  public isDoneStep2: any = 'disableDiv';
  public checkImgTop: any = false;
  public checkImgBottom: any = false;
  // Send active template promo
  public isActiveTemplateUpdate: any = false;
  public isDoneStep3: any = false;
  // update template
  public isTemplate01: any = true;
  public titlePromo: any = 'Select Template';
  public isUpdatePromo: any = false;
  public template_id: any = '';
  // id template detail
  public id_1_templateDetail: any = '';
  public id_2_templateDetail: any = '';
  public id_3_templateDetail: any = '';
  public id_4_templateDetail: any = '';
  // choose template
  public landingPagePromo: PromoTemplate = new PromoTemplate();
  public loginPromo: PromoTemplate = new PromoTemplate();
  public landingDesktop: PromoTemplate = new PromoTemplate();
  public loginDesktop: PromoTemplate = new PromoTemplate();

  // public choose radient
  public isChooseRadientLanding: any = 'disable';
  public isChooseImageLanding: any = '';
  public isChooseRadientLoginTemplate: any = 'disable';
  public isChooseImageLoginTemplate: any = '';
  public isChooseRadientLoginDesk: any = 'disable';
  public isChooseImageLoginDesk: any = '';
  public isChooseRadientLandingDesk: any = 'disable';
  public isChooseImageLandingDesk: any = '';

  // gradient choosed
  public gradientChoosedLanding: any = '';
  public gradientChoosedLogin: any = '';
  public gradientChoosedLandingDesk: any = '';
  public gradientChoosedLoginDesk: any = '';
  // gradient top and body
  public gradientImage: any = '';
  public location_id: any = '';
  public location_id_update: any = '';

  constructor(public feedService: FeedsService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService,
              private ruleService: RuleService,
              private searchProfileService: SearchProfileService,
              private route: ActivatedRoute) {
    this.IP = config.getIpInSight();
    this.route.params.subscribe(params => {
      this.location_id = params.id;
    });
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    const that = this;
    // CK editor
    const ed1 = ckEditor('link_text');
    // get value of ckeditor
    ed1.on('change', function () {
      const value = this.getData();
      that.loginPromo.link_text = value;
    });
    const ed2 = ckEditor('term_condition');
    // get value of ckeditor
    ed2.on('change', function () {
      const value = this.getData();
      that.loginPromo.term_condition = value;
      $('#term_template').innerHTML = value;
    });
    const ed3 = ckEditor('link_text_desk');
    // get value of ckeditor
    ed3.on('change', function () {
      const value = this.getData();
      that.loginDesktop.link_text = value;
    });
    const ed4 = ckEditor('term_condition_desk');
    // get value of ckeditor
    ed4.on('change', function () {
      const value = this.getData();
      that.loginDesktop.term_condition = value;
      $('#term_template_desk').innerHTML = value;
    });

    setHeightElement('#external');
    removeclass();
    show_anypicker();
    show_table_emojiAddfeed('addFeed');
    show_table_emojiAddfeed('updateFeed');
    // check image Feed
    checkImageFeed('#margin');
    checkImageFeed('#margin_update');
    // check image template
    checkImageTemplate('#filePromo_login_template', 1, 900, 1600); // check for login mobile
    checkImageTemplate('#filePromo_landing_template', 2, 900, 1600); // check for landing mobile
    checkImageTemplate('#filePromo_login_template_desk', 3, 1600, 900); // check for login desktop
    checkImageTemplate('#image_landing_desk', 4, 1600, 600); // check for landing desktop
    $('.carousel').carousel({
      interval: 0
    });
    // Remove event on calendar and on month
    $(document).on('click', '.remove_event', function () {
      that.isUpdate = false;
      const id = $(this).data('id');
      that.deleteEvent(id);
    });

    $(document).on('click', '.remove_item', function () {
      that.isUpdate = false;
    });
    // End remove event on calendar and on month
    // Reset to Add promo
    $(document).on('click', '#add_feed_button', function () {
      that.isUpdatePromo = false;
      $('#step1').click();
      this.titlePromo = 'Select Template';
      that.resetStep2();
      that.resetStep03();
      $('.content-item_01').removeClass('active');
    });
    // End reset to add promo
    // Click update event on calendar
    $(document).on('click', '.content_event', function () {
      const type = $(this).data('type');
      if (type === 1) {
        if (that.isUpdate) {
          const id = $(this).children('.remove_event').data('id');
          that.idUpdate = id;
          that.getFeedById(id);
          $('#modal_updateFeed').click();
        }
      } else {
        if (that.isUpdate) {
          that.resetStep2();
          that.isUpdatePromo = true;
          that.titlePromo = 'Update Template';
          that.isActiveTemplateUpdate = false;
          $('.button_active_template ').removeClass('active');
          const id = $(this).children('.remove_event').data('id');
          that.idUpdate = id;
          that.getPromoById(id);
          $('#modal_updatePromo').click();
        }
      }
      that.isUpdate = true;
    });
    // End update event on calendar
    $(document).on('click', '.item_list', function () {
      const type = $(this).data('type');
      const template_id = $(this).data('template');
      if (type === 1) {
        if (that.isUpdate) {
          console.log(that.idUpdate);
          that.getFeedById(that.idUpdate);
          $('#modal_updateFeed').click();
        }
      } else {
        if (that.isUpdate) {
          that.template_id = template_id;
          that.isUpdatePromo = true;
          console.log(template_id);
          that.titlePromo = 'Update Template';
          that.getPromoById(that.idUpdate);
          $('#modal_updatePromo').click();
        }
      }

      that.isUpdate = true;
    });
    // end update envent on feed month
    // template
    // Action step 01 select template
    $(document).on('click', '.content-item_01', function () {
      $('.content-item_01').removeClass('active');
      $(this).addClass('active');
      $('#step2').click();
    });
    // End select template
    // active top and bottom template
    $(document).on('click', '.group_item', function () {
      $('.group_item').removeClass('active');
      $(this).addClass('active');
    });
    $(document).on('click', '.temp_03', function () {
      $('.temp_03').removeClass('active');
      $(this).addClass('active');
    });

    this.showTemplateDemo('#click_note_login', '#content-note-login', 1);
    this.showTemplateDemo('#click_note_login_desk', '#content-note-login_desk', 1);
    this.showTemplateDemo('#click_note_landing', '#content-note-landing', 1);
    this.showTemplateDemo('#click_note_landing_desk', '#content-note-landing_desk', 1);
    this.getAllFeed();
    this.getFeedsMonth();
    getselect2(this.IP);
    // Call function promo
    this.getAllTemplate();
    // carousel feed
    $('#modal-addPromo .carousel-control-prev .left').css('display', 'block!important');

    $('#modal-addPromo .carousel').on('slid.bs.carousel', function () {
      if ($('#modal-addPromo .carousel-item:first').hasClass('active')) {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Select Template';
        }
        $('#modal-addPromo .carousel-control-prev').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel-control-next').addClass('d-block').removeClass('d-none');
        $('#modal-addPromo .carousel_control .left').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel_control .right').addClass('d-block').removeClass('d-none');
        // console.log($this.data);
      } else if ($('#modal-addPromo .carousel-item:last').hasClass('active')) {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Launch Setting';
        }
        $('#modal-addPromo .carousel-control-next').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel-control-prev').addClass('d-block').removeClass('d-none');
        $('#modal-addPromo .carousel_control .right').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel_control .left').addClass('d-block').removeClass('d-none');
      } else {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Select Media';
        }

        $('#modal-addPromo .carousel-control-prev').removeClass('d-none').addClass('d-block');
        $('#modal-addPromo .carousel-control-next').removeClass('d-none').addClass('d-block');
        $('#modal-addPromo .carousel-control').removeClass('d-none').addClass('d-block');
      }
    });

  }

  setStep() {
    this.step_02 = 'disableDiv';
    this.step_03 = 'disableDiv';
  }

  // show template
  showTemplateDemo(objHover, objContent, type) {
    const that = this;
    let isLeave = false;
    // Note download template demo
    $(objHover).mouseover(function () {
      isLeave = true;
      that.getImgTemplateDemo(type);
      $(objContent).removeClass('d-none').addClass('d-inline-block');
    });
    $(objContent).mouseover(function () {
      if (isLeave) {
        $(this).removeClass('d-none').addClass('d-inline-block');
      } else {
        $(this).removeClass('d-inline-block').addClass('d-none');
      }
    });

    $(objContent).mouseleave(function () {
      $(this).removeClass('d-inline-block').addClass('d-none');
    });

  }

  checkLengthString(type, input, length, pos) {
    const str = escapeRegExp(input.value);
    // input.value = input.value.trim();
    const lengthCut = length + input.value.length - str.length;
    if (str.length > length) {
      input.value = input.value.slice(0, lengthCut);
      switch (type) {
        case 1: {
          if (pos === 1) {
            this.loginPromo.name = input.value;
          }
          if (pos === 2) {
            this.loginPromo.phone = input.value;
          }
          if (pos === 3) {
            this.loginPromo.button = input.value;
          }
          if (pos === 4) {
            this.loginPromo.link_text = input.value;
          }
          break;
        }
        case 2: {
          if (pos === 3) {
            this.landingPagePromo.button = input.value;
          }
          break;
        }
        case 3: {
          if (pos === 1) {
            this.loginDesktop.name = input.value;
          }
          if (pos === 2) {
            this.loginDesktop.phone = input.value;
          }
          if (pos === 3) {
            this.loginDesktop.button = input.value;
          }
          if (pos === 4) {
            this.loginDesktop.link_text = input.value;
          }
          break;
        }
        case 4: {
          if (pos === 3) {
            this.landingDesktop.button = input.value;
          }
          break;
        }


      }
    }
  }

  getTemplateInfo(id, url) {
    this.resetStep2();
    this.idTemplate = id;
    this.checkTemplateId(id);
    $('.img_last_template').attr('src', url);
  }

  checkHoverTemplate(id) {
    $('.sub_templates_demo').addClass('d-none');
    const _id = '#subTemplates_' + id;
    this.getTemplateDemo(id);
    $(_id).removeClass('d-none');
  }

  checkLeaveTemplate(id) {
    const _id = '#subTemplates_' + id;
    $(_id).addClass('d-none');
  }

  checkTemplateId(id) {
    this.getGradientImages();
    if (id === 1) {
      this.isTemplate01 = true;
      $('#img_template_demo_landing').attr('src', './assets/images/template_insight/landing_temp01_final.png');
      $('#img_template_demo_landing_donwload').attr('href', './assets/images/template_insight/landing_temp01_final.png');

    } else {
      if (id === 2) {
        $('#img_template_demo_landing').attr('src', './assets/images/template_insight/landing_temp02_final.png');
        $('#img_template_demo_landing_donwload').attr('href', './assets/images/template_insight/landing_temp02_final.png');
        this.isTemplate01 = false;
        this.loginPromo.resetAttribute();
      }
    }
    $('#img_template_demo_landing_desk').attr('src', './assets/images/template_insight/landing_temp01_final_desk.png');
    $('#landing_donwload_desk').attr('href', './assets/images/template_insight/landing_temp01_final_desk.png');
  }

  // Api for Promo
  // 1 get all template
  getAllTemplate() {
    this.feedService.getAllTemplate().subscribe(res => {
      this.templates = res.data;
    }, err => {
      console.log(err);
    });
  }

  getTemplateDemo(id) {
    this.feedService.getTemplateDemo(id).subscribe(res => {
      this.templatesDemo = res.data;
    }, err => {
      console.log(err);
    });
  }

  getImgTemplateDemo(type) {
    this.feedService.getImageDemo(this.idTemplate, type).subscribe(res => {
      if (type === 1) {
        this.imgTemplateType1 = res.data[0].file;
      } else {
        this.imgTemplateType2 = res.data[0].file;
      }
    }, err => {
      console.log(err);
    });
  }

  chooseImage(pos, obj) {
    switch (pos) {
      case 1: {
        this.isChooseImageLoginTemplate = '';
        this.isChooseRadientLoginTemplate = 'disable';
        break;
      }
      case 2: {
        this.isChooseImageLanding = '';
        this.isChooseRadientLanding = 'disable';
        break;
      }

      case 3: {
        this.isChooseImageLoginDesk = '';
        this.isChooseRadientLoginDesk = 'disable';
        break;
      }
      case 4: {
        this.isChooseImageLandingDesk = '';
        this.isChooseRadientLandingDesk = 'disable';
        break;
      }
    }
    // if (pos === 1) {
    //   this.isChooseImageLanding = '';
    //   this.isChooseRadientLanding = 'disable';
    // } else {
    //   this.isChooseImageLoginTemplate = '';
    //   this.isChooseRadientLoginTemplate = 'disable';
    // }
    $(obj).css('opacity', 0);
  }

  chooseRadient(pos, obj) {
    switch (pos) {
      case 1: {
        this.isChooseImageLoginTemplate = 'disable';
        this.isChooseRadientLoginTemplate = '';
        break;
      }
      case 2: {
        this.isChooseImageLanding = 'disable';
        this.isChooseRadientLanding = '';
        break;
      }

      case 3: {
        this.isChooseImageLoginDesk = 'disable';
        this.isChooseRadientLoginDesk = '';
        break;
      }
      case 4: {
        this.isChooseImageLandingDesk = 'disable';
        this.isChooseRadientLandingDesk = '';
        break;
      }
    }
    $(obj).css('opacity', 1);
  }

  gradientChoosedAction(pos, obj, file, id) {
    switch (pos) {
      case 1: {
        $(obj).attr('src', file);
        this.gradientChoosedLogin = id;
        break;
      }
      case 2: {
        $(obj).attr('src', file);
        this.gradientChoosedLanding = id;
        break;
      }
      case 3: {
        $(obj).attr('src', file);
        this.gradientChoosedLoginDesk = id;
        break;
      }
      case 4: {
        $(obj).attr('src', file);
        this.gradientChoosedLandingDesk = id;
        break;
      }
    }
    // this.checkForStep2();
  }

  getGradientImages() {
    // call one times to get 4 gradient
    this.feedService.getGradientImageFunction().subscribe(res => {
      this.gradientImage = res.data;
    }, err => {
      console.log(err);
    });
  }


  addPromoTemplate(fileLogin, fileLanding, fileLoginDesk, fileLandingDesk, title, dateStart, dateEnd, timeStart, timeEnd) {
    const that = this;
    const datas = new FormData();
    // append file or gradient for login template
    if (this.isChooseRadientLoginTemplate) {
      datas.append('file1', fileLogin.files[0]);
      datas.append('url1', '');
    } else {
      datas.append('file1', null);
      datas.append('url1', this.gradientChoosedLogin);
    }
    // append file or gradient for landing template
    if (this.isChooseRadientLanding) {
      datas.append('file2', fileLanding.files[0]);
      datas.append('url2', '');
    } else {
      datas.append('file2', null);
      datas.append('url2', this.gradientChoosedLanding);
    }
    // append file or gradient for login template
    if (this.isChooseRadientLoginDesk) {
      datas.append('file3', fileLoginDesk.files[0]);
      datas.append('url3', '');
    } else {
      datas.append('file3', null);
      datas.append('url3', this.gradientChoosedLoginDesk);
    }
    // append file or gradient for login template
    if (this.isChooseRadientLandingDesk) {
      datas.append('file4', fileLandingDesk.files[0]);
      datas.append('url4', '');
    } else {
      datas.append('file4', null);
      datas.append('url4', this.gradientChoosedLandingDesk);
    }
    // append content for template
    const content1 = {
      'hoten': this.loginPromo.name,
      'phone': this.loginPromo.phone,
      'button': this.loginPromo.button,
      'link_text': this.loginPromo.link_text,
      'term_condition': this.loginPromo.term_condition
    };
    const content2 = {
      'button': this.landingPagePromo.button
    };
    const content3 = {
      'hoten': this.loginDesktop.name,
      'phone': this.loginDesktop.phone,
      'button': this.loginDesktop.button,
      'link_text': this.loginDesktop.link_text,
      'term_condition': this.loginDesktop.term_condition
    };
    const content4 = {
      'button': this.landingDesktop.button
    };


    datas.append('content1', JSON.stringify(content1));
    datas.append('content2', JSON.stringify(content2));
    datas.append('content3', JSON.stringify(content3));
    datas.append('content4', JSON.stringify(content4));
    // append template
    datas.append('title', title.value);
    datas.append('location_id', this.location_id);
    datas.append('date_start', dateStart.value);
    datas.append('date_end', dateEnd.value);
    datas.append('time_start', timeStart.value);
    datas.append('time_end', timeEnd.value);
    datas.append('description', '');
    datas.append('template_id', this.idTemplate);
    datas.append('location_id', this.location_id);

    that.feedService.addPromo(datas).subscribe(res => {
      swal('Add Success!', '', 'success');
      this.getFeedsMonth();
      this.getAllFeed();
      // reset field
      this.resetStep2();
      this.resetStep03();
      this.titlePromo = 'Select Template';
      $('#closeAddPromo').click();
    }, err => {
      console.log(err);
      swal(err.error.message, '', 'error');
    });

  }

  updatePromoTemplate(fileLogin, fileLanding, fileLoginDesk, fileLandingDesk, title, dateStart, dateEnd, timeStart, timeEnd) {
    const that = this;
    const datas = new FormData();
    // append file or gradient for login template
    if (this.isChooseRadientLoginTemplate) {
      datas.append('file1', fileLogin.files[0]);
      datas.append('url1', '');
    } else {
      datas.append('file1', null);
      datas.append('url1', this.gradientChoosedLogin);
    }
    // append file or gradient for landing template
    if (this.isChooseRadientLanding) {
      datas.append('file2', fileLanding.files[0]);
      datas.append('url2', '');
    } else {
      datas.append('file2', null);
      datas.append('url2', this.gradientChoosedLanding);
    }
    // append file or gradient for login template
    if (this.isChooseRadientLoginDesk) {
      datas.append('file3', fileLoginDesk.files[0]);
      datas.append('url3', '');
    } else {
      datas.append('file3', null);
      datas.append('url3', this.gradientChoosedLoginDesk);
    }
    // append file or gradient for login template
    if (this.isChooseRadientLandingDesk) {
      datas.append('file4', fileLandingDesk.files[0]);
      datas.append('url4', '');
    } else {
      datas.append('file4', null);
      datas.append('url4', this.gradientChoosedLandingDesk);
    }
    // append content for template
    const content1 = {
      'hoten': this.loginPromo.name,
      'phone': this.loginPromo.phone,
      'button': this.loginPromo.button,
      'link_text': this.loginPromo.link_text,
      'term_condition': this.loginPromo.term_condition
    };
    const content2 = {
      'button': this.landingPagePromo.button
    };
    const content3 = {
      'hoten': this.loginDesktop.name,
      'phone': this.loginDesktop.phone,
      'button': this.loginDesktop.button,
      'link_text': this.loginDesktop.link_text,
      'term_condition': this.loginDesktop.term_condition
    };
    const content4 = {
      'button': this.landingDesktop.button
    };


    datas.append('content1', JSON.stringify(content1));
    datas.append('content2', JSON.stringify(content2));
    datas.append('content3', JSON.stringify(content3));
    datas.append('content4', JSON.stringify(content4));
    // append template
    datas.append('title', title.value);
    datas.append('location_id', this.location_id_update);
    datas.append('date_start', dateStart.value);
    datas.append('date_end', dateEnd.value);
    datas.append('time_start', timeStart.value);
    datas.append('time_end', timeEnd.value);
    datas.append('description', '');
    datas.append('feed_data_id', this.idUpdate);
    datas.append('template_detail_id_1', this.id_1_templateDetail);
    datas.append('template_detail_id_2', this.id_2_templateDetail);
    datas.append('template_detail_id_3', this.id_3_templateDetail);
    datas.append('template_detail_id_4', this.id_4_templateDetail);
    that.feedService.updateProm(datas).subscribe(res => {
      this.getFeedsMonth();
      this.getAllFeed();
      // reset field
      this.resetStep2();
      this.resetStep03();
      this.titlePromo = 'Select Template';
      swal('Update Success!', '', 'success');
      $('#closeAddPromo').click();
    }, err => {
      swal(err.error.message, '', 'error');
    });
  }

  changeTileTemplate() {
    this.checkForStep2();
  }

  // check do step 2
  checkForStep2() {
    const isGradientTop = this.gradientChoosedLanding === '' ? false : true;
    const isGradientBottom = this.gradientChoosedLogin === '' ? false : true;
    if (this.idTemplate === 1) {
      if ((this.checkImgTop || isGradientTop) && (this.checkImgBottom || isGradientBottom)) {
        this.isDoneStep2 = true;
        $('#carouseStep3').removeClass('disableDiv');
      } else {
        this.isDoneStep2 = false;
        $('#carouseStep3').removeClass('disableDiv');
      }
    } else {
      if ((this.checkImgTop || isGradientTop)) {
        this.isDoneStep2 = true;
        $('#carouseStep3').removeClass('disableDiv');
      } else {
        this.isDoneStep2 = false;
        $('#carouseStep3').removeClass('disableDiv');
      }
    }

  }

  // check do step 3
  checkForStep3(title) {
    if (title.value) {
      $('.launch_promo_action').removeClass('disableDiv');
    } else {
      $('.launch_promo_action').addClass('disableDiv');
    }
  }


  // Reset field Promo
  resetStep2() {
    // template landing
    this.landingPagePromo.resetAttribute();
    CKEDITOR.instances.link_text.setData('Link text');
    CKEDITOR.instances.term_condition.setData('Term and condition content');
    CKEDITOR.instances.link_text_desk.setData('Link text');
    CKEDITOR.instances.term_condition_desk.setData('Term and condition content');
    // reset login field template
    $('#filePromo_login_template').val('');
    $('#lable-file-login_template').val('Upload Image');
    $('.img_login_template').attr('src', '');
    $('.gradient_login').attr('src', '');
    $('#check_image_login_template').click();
    this.loginPromo.resetAttribute();
    // template landing
    $('#filePromo_landing_template').val('');
    $('#lable-file-promo_02').val('Upload Image');
    $('.img_landing_template').attr('src', '');
    $('.gradient_landing_template').attr('src', '');
    $('#check_image_landing').click();
    this.landingPagePromo.resetAttribute();
    // template login desktop
    $('#filePromo_login_template_desk').val('');
    $('#lable-file-login_desk').val('Upload Image');
    $('.img_login_desk').attr('src', '');
    $('.gradient_login_check_desk').attr('src', '');
    $('#check_image_login_template_desk').click();
    this.loginDesktop.resetAttribute();
    // template landing desktop
    $('#image_landing_desk').val('');
    $('#lable-image_landing_desk').val('Upload Image');
    $('.img_landing_template_desk').attr('src', '');
    $('.gradient_landing_template_desk').attr('src', '');
    $('#check_image_landing_desk').click();
    this.landingDesktop.resetAttribute();
    $('.gradient_item').removeClass('active');
    // reset gradient
    this.gradientChoosedLanding = '';
    this.gradientChoosedLogin = '';
    this.gradientChoosedLoginDesk = '';
    this.gradientChoosedLandingDesk = '';

  }

  resetStep03() {
    $('#title_promo').val('');
    $('#location').val('');
    $('#start_day_promo').val('');
    $('#end_day_promo').val('');
    $('#time_start_promo').val('');
    $('#time_end_promo').val('');
    $('#step1').click();
    $('.content-item_01').removeClass('active');
  }


  getPromoById(id) {
    this.feedService.getTemplateById(id).subscribe(res => {
      // check type template
      console.log(res);
      this.checkTemplateId(res.feed_data.template_id);
      this.resetFieldUpdatePromo(res);
    }, err => {
      console.log(err);
    });
  }

  resetFieldUpdatePromo(res) {
    const that = this;
    $('#step2').click();
    $('#carouseStep3').removeClass('disableDiv');
    this.idTemplate = res.feed_data.template_id;
    this.location_id_update = res.feed_data.location_id;
    $('.img_last_template').attr('src', res.feed_data.file);
    this.resetUpdateTempalte01(res);
  }

  resetUpdateTempalte01(data) {
    const that = this;
    data.template.forEach(function (value) {
      switch (value.type) {
        case 1: {
          that.id_1_templateDetail = value.id;
          // process gradient
          if (value.gradient) {
            const idGradient = '#gradient_login_template_' + value.gradient;
            $('.gradient_login_check').css('opacity', '1');
            $('.gradient_login').attr('src', value.file);
            $('#check_gradient_login_template').click();
            setTimeout(function () {
              $(idGradient).click();
              $(idGradient).addClass('active');
            }, 600);
            that.isChooseRadientLoginTemplate = '';
            that.isChooseImageLoginTemplate = 'disable';
          } else {
            $('#check_image_login_template').click();
            $('.img_login_template').css('display', 'inline-block');
            $('.img_login_template').attr('src', value.file);
            that.isChooseRadientLoginTemplate = 'disable';
            that.isChooseImageLoginTemplate = '';
          }
          value.content = JSON.parse(value.content);
          value.content = JSON.parse(value.content);
          that.loginPromo.setAttr(value.content.hoten, value.content.phone, value.content.button, value.content.link_text, value.content.term_condition);
          CKEDITOR.instances.link_text.setData(value.content.link_text);
          CKEDITOR.instances.term_condition.setData(value.content.term_condition);
          setTimeout(function () {
            CKEDITOR.instances.link_text.setData(value.content.link_text);
            CKEDITOR.instances.term_condition.setData(value.content.term_condition);
          }, 800);
          break;
        }
        case 2: {
          that.id_2_templateDetail = value.id;
          if (value.gradient) {
            const idGradient = '#gradient_landing_' + value.gradient;
            $('.gradient_landing_check').css('opacity', '1');
            $('.gradient_landing_template').attr('src', value.file);
            $('#check_gradient_landing').click();
            setTimeout(function () {
              $(idGradient).click();
              $(idGradient).addClass('active');
            }, 600);
            that.isChooseRadientLanding = '';
            that.isChooseImageLanding = 'disable';
          } else {
            $('.img_landing_template').css('display', 'inline-block');
            $('.img_landing_template').attr('src', value.file);
            $('#check_image_landing').click();
            that.isChooseRadientLanding = 'disable';
            that.isChooseImageLanding = '';
          }
          value.content = JSON.parse(value.content);
          value.content = JSON.parse(value.content);
          that.landingPagePromo.setAttr('', '', value.content.button, '', '');
          break;
        }
        case 3: {
          that.id_3_templateDetail = value.id;
          // process gradient
          if (value.gradient) {
            const idGradient = '#gradient_login_template_desk_' + value.gradient;
            $('.gradient_login_check_desk').css('opacity', '1');
            $('.gradient_login_check_desk').attr('src', value.file);
            $('#check_gradient_login_template_desk').click();
            setTimeout(function () {
              $(idGradient).click();
              $(idGradient).addClass('active');
            }, 600);
            that.isChooseRadientLoginDesk = '';
            that.isChooseImageLoginDesk = 'disable';
          } else {
            $('#check_image_login_template_desk').click();
            $('.img_login_desk').css('display', 'inline-block');
            $('.img_login_desk').attr('src', value.file);
            that.isChooseRadientLoginDesk = 'disable';
            that.isChooseImageLoginDesk = '';
          }
          value.content = JSON.parse(value.content);
          value.content = JSON.parse(value.content);
          that.loginDesktop.setAttr(value.content.hoten, value.content.phone, value.content.button, value.content.link_text, value.content.term_condition);
          CKEDITOR.instances.link_text_desk.setData(value.content.link_text);
          CKEDITOR.instances.term_condition_desk.setData(value.content.term_condition);
          setTimeout(function () {
            CKEDITOR.instances.link_text_desk.setData(value.content.link_text);
            CKEDITOR.instances.term_condition_desk.setData(value.content.term_condition);
          }, 800);
          break;
        }
        case 4: {
          that.id_4_templateDetail = value.id;
          if (value.gradient) {
            const idGradient = '#gradient_landing_desk_' + value.gradient;
            $('.gradient_landing_template_check_desk').css('opacity', '1');
            $('.gradient_landing_template_desk').attr('src', value.file);
            $('#check_gradient_landing_desk').click();
            setTimeout(function () {
              $(idGradient).click();
              $(idGradient).addClass('active');
            }, 600);
            that.isChooseRadientLandingDesk = '';
            that.isChooseImageLandingDesk = 'disable';
          } else {
            $('.img_landing_template_desk').css('display', 'inline-block');
            $('.img_landing_template_desk').attr('src', value.file);
            $('#check_image_landing_desk').click();
            that.isChooseRadientLandingDesk = 'disable';
            that.isChooseImageLandingDesk = '';
          }
          value.content = JSON.parse(value.content);
          value.content = JSON.parse(value.content);
          that.landingDesktop.setAttr('', '', value.content.button, '', '');
          break;
        }
      }
    });
    $('#carouseStep3').removeClass('disableDiv');
    $('.launch_promo_action').removeClass('disableDiv');
    // DONE STEP 3
    $('#title_promo').val(data.feed_data.title);
    $('#start_day_promo').val(data.feed_data.date_start);
    $('#end_day_promo').val(data.feed_data.date_end);
    $('#time_start_promo').val(data.feed_data.time_start);
    $('#time_end_promo').val(data.feed_data.time_end);
    $('#launch_promo').removeClass('disableDiv');
  }


  getIdEventMonth(id) {
    this.idUpdate = id;
  }

  getAllFeed() {
    const that = this;
    // get All Data
    this.subcription1 = this.feedService.getAllFeed(this.location_id).subscribe(res => {
      this.isLoadingCalendar = false;
      $('#calendar').fullCalendar('destroy');
      feedFunction(res.data);
      show_icon(res.data);
    }, error1 => {
      console.log(error1);
    });
  }

  getFeedsMonth() {
    const datas = [];
    this.subcription2 = this.feedService.getFeedsWithMonth(this.location_id).subscribe(res => {
      $.each(res.data, function (index, item) {
        datas.push({
          title: index,
          event: item
        });
      });
      this.dataMonth = datas;
    }, error1 => {
      console.log(error1);
    });
  }

  // Add Feed
  addFeed(file, dateStart, dateEnd, timeStart, timeEnd, favorite) {
    this.feed.date_start = dateStart.value;
    this.feed.date_end = dateEnd.value;
    this.feed.time_start = timeStart.value;
    this.feed.time_end = timeEnd.value;
    this.feed.favorite = $('#favorite_feed').val();
    this.feed.feed_emoji = $('#content_chat_addFeed').html();
    this.feed.description = this.convertString('#content_chat_addFeed');
    if (!file.files[0]) {
      this.input.append('file', null);
    } else {
      this.input.append('file', file.files[0]);
    }
    this.input.append('title', this.feed.title);
    this.input.append('description', this.feed.description);
    this.input.append('date_start', this.feed.date_start);
    this.input.append('date_end', this.feed.date_end);
    this.input.append('time_start', this.feed.time_start);
    this.input.append('time_end', this.feed.time_end);
    this.input.append('favorite', this.feed.favorite);
    this.input.append('time_repeat', '0');
    this.input.append('feed_emoji', this.feed.feed_emoji);
    this.input.append('location_id', this.location_id);

    this.feedService.addFeed(this.input).subscribe(res => {

      this.getFeedsMonth();
      this.getAllFeed();
      $('.close').click();
      swal('Add Success!', '', 'success');
      this.input = new FormData();
      this.feed = new Feed();
      dateStart.value = '';
      dateEnd.value = '';
      timeStart.value = '';
      timeEnd.value = '';
      $('#margin').val('');
      $('#favorite_feed').val('');
      $('#favorite_feed').select2();
      $('.blah0_addFeed').attr('src', '').removeAttr('style');
      $('.blah0_addFeed_plus_img').css('display', 'inline-block');
      $('#btn_addimg').css('display', 'block');
      $('#content_chat_addFeed').html('');
      $('#add_img').css('display', 'inline-block');
      css();

    }, error => {
      this.getFeedsMonth();
      this.getAllFeed();
      console.log(error);
      swal(error.error.message, '', 'error');

    });

  }

  // update Feed Action
  updateFeedAction(file, date_start, date_end, time_start, time_end) {
    this.updateFeed.feed_emoji = $('#content_chat_updateFeed').html();
    this.updateFeed.description = this.convertStringUpdate('#content_chat_updateFeed');
    if (file.files[0]) {
      this.updateFeed.file = file.files[0];
    }
    if (!this.updateFeed.title) {
      this.updateFeed.title = this.feedById.title;
    }
    if (!this.updateFeed.description) {
      this.updateFeed.description = this.feedById.description;
    }
    this.updateFeed.favorite = $('#favorite_feedUpdate').val().toString();

    if (!date_start) {
      this.inputUpdate.append('date_start', null);
    } else {
      this.inputUpdate.append('date_start', date_start.value);
    }

    if (!date_end) {
      this.inputUpdate.append('date_end', null);
    } else {
      this.inputUpdate.append('date_end', date_end.value);
    }

    if (!time_start) {
      this.inputUpdate.append('time_start', null);
    } else {
      this.inputUpdate.append('time_start', time_start.value);

    }
    if (!time_end) {
      this.inputUpdate.append('time_end', null);
    } else {
      this.inputUpdate.append('time_end', time_end.value);
    }


    this.inputUpdate.append('title', this.updateFeed.title);
    this.inputUpdate.append('description', this.updateFeed.description);
    this.inputUpdate.append('favorite', this.updateFeed.favorite);
    this.inputUpdate.append('file', this.updateFeed.file);
    this.inputUpdate.append('time_repeat', this.updateFeed.time_repeat);
    this.inputUpdate.append('feed_emoji', this.updateFeed.feed_emoji);

    this.feedService.updateFeed(this.idUpdate, this.inputUpdate).subscribe(res => {
      $('.close').click();
      swal('Update Success!', '', 'success');
      $('#calendar').fullCalendar('destroy');
      this.getAllFeed();
      this.getFeedsMonth();
      $('.fc-view-container').css('display', 'block');
      $('.card_list').css('display', 'none');
    }, err => {
      swal(err.error.message, '', 'error');
    });
    // reset fields
    $('#margin_update').val('');
    this.inputUpdate = new FormData();
    this.updateFeed = new Feed();
    date_start.value = '';
    date_end.value = '';

  }

  // convert chat to back end
  convertString(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ').replace(/<div>|<[/]div>/g, '')
      .replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
    return result;
  }

  convertStringUpdate(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiupdateFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/<div>|<[/]div>/g, '')
      .replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=');
    return result;
  }

// check image feed
  checkImageFeed() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isFeedImage = isImageFeed;
    }, 20);
  }

  // check image feed
  checkImageTemplate(file, i) {
    const that = this;
    setTimeout(function () {
      console.log(3);
      switch (i) {
        case 1: {
          that.isLoginImage = isImageLogin;
          break;
        }
        case 2: {
          that.isLandingImage = isImageLanding;
          break;
        }
        case 3: {
          that.isLoginImageDesk = isImageLoginDesk;
          break;
        }
        case 4: {
          that.isLandingImageDesk = isImageLandingDesk;
          break;
        }
      }
    }, 100);
  }

  deleteEvent(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Event!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteEvent(id).subscribe(res => {
            this.getFeedsMonth();
            $('#calendar').fullCalendar('destroy');
            this.getAllFeed();
            monthClick();
            swal('Delete Success!', '', 'success');
          }, error => {
            swal(error.error.message, '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  // Update Feed
  getFeedById(id) {
    this.feedService.getFeedById(id).subscribe(res => {
      this.feedById = res.data;
      this.feed.time_repeat = this.feedById.time_repeat;
      console.log(this.feedById);
      this.setDataFieldInputUpdate();
    }, err => {
      console.log(err.error.message);
    });
  }

  // flow data to modal box update feed
  setDataFieldInputUpdate() {
    css();
    const recurrent = this.feedById.time_repeat;
    const objClick = '.recurrence-' + recurrent;
    $(objClick).click();
    $('.blah0_updateFeed_plus_img').css('display', 'none');
    $('.blah0_updateFeed_add_img').css('display', 'none');
    $('#img_file').attr('src', this.feedById.file);
    $('#title_update').val(this.feedById.title);
    $('#content_chat_updateFeed').html(this.feedById.feed_emoji);
    this.updateFeed.date_start = this.feedById.date_start;
    this.updateFeed.date_end = this.feedById.date_end;
    this.updateFeed.time_start = this.feedById.time_start;
    this.updateFeed.time_end = this.feedById.time_end;
    setTag(this.idUpdate, this.IP);
  }


  checkDateEvent(date_start, date_end) {
    if (!date_start && !date_end) {
      return true;
    }
    if (!date_start || !date_end) {
      return false;
    }
    if (date_start > date_end) {
      return false;
    } else {
      return true;
    }
  }

  checkDateTemplatePromo(date_start, date_end, time_start, time_end) {
    if (!date_start && !date_end && !time_end && !time_end) {
      return false;
    }
    if (!date_start || !date_end || !time_start || !time_end) {
      return false;

    }
    if (date_start > date_end) {
      return false;
    } else {
      if (date_start === date_end) {
        if (time_start >= time_end) {
          return false;
        } else {
          this.isDoneStep3 = true;
          return true;
        }
      } else {
        this.isDoneStep3 = true;
        return true;
      }
    }
  }

  ngOnDestroy() {
    this.subcription1.unsubscribe();
    this.subcription2.unsubscribe();
  }

  // dataTable function
  dataTablesConfig(objTable, objPagi) {
    // You'll have to wait that changeDetection occurs and projects data into
    // the HTML template, you can ask Angular to that for you ;-)
    this.chRef.detectChanges();
    $(objTable).DataTable({
      'pageLength': 10
    });
    this.setPagiInfo(objTable, objPagi);
    this.actionPagiPage(objTable, objPagi);
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    if (table && table.page.info()) {
      const currentPage = table.page.info().page + 1;
      const totalPage = table.page.info().pages;
      const pagi = 'Page ' + currentPage + ' of ' + totalPage;
      $(objPagi + ' .pagin').html(pagi);
    } else {
      $(objPagi + ' .pagin').html('Page 1 of 1');
    }
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }

}

// set value for ckedtior (link_text, term_condition)
// CKEDITOR.instances.link_text.setData( 'Link text');
// CKEDITOR.instances.term_condition.setData( 'Term and condition content');
