import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ConfigIpService} from '../configIP/config-ip.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private ip:  any = '';
  private ipInSight:  any = '';
  private urlGetTimesZone = '/api/timezone';
  private urlGetPersonOnMap = '/api/dashboard/coordinates_device';
  private urlGetZonePosition = '/api/statitic/get_zone';

  constructor(private http: HttpClient, private configIp: ConfigIpService) {
    this.ip = configIp.getIp();
    this.ipInSight = configIp.getIpInSight();
    this.urlGetTimesZone = this.ip + this.urlGetTimesZone;
    this.urlGetPersonOnMap = this.ipInSight + this.urlGetPersonOnMap;
    this.urlGetZonePosition = this.ip + this.urlGetZonePosition;
  }

  getTimeZone(): Observable<any> {
    return this.http.get<any>(this.urlGetTimesZone);
  }

  getCurrentPersonOnMap(data): Observable<any> {
    return this.http.post(this.urlGetPersonOnMap, {
      'location': data
    });
  }
  getZonePosition(): Observable<any> {
    return this.http.get<any>(this.urlGetZonePosition);
  }
}
