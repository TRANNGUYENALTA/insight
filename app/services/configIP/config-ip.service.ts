import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigIpService {

  constructor() {
  }

  getIpInSight() {
    return 'http://insight.dev-altamedia.com';
  }

  getIp() {
    return 'http://192.168.11.240';
  }
}
