import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigIpService} from '../configIP/config-ip.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MailService {
  private urlSendMail = '/api/statitic/send_mail';

  constructor(private http: HttpClient, private ip: ConfigIpService) {
    this.urlSendMail = ip.getIp() + this.urlSendMail;
  }

  sendMail(data): Observable<any> {
    return this.http.post(this.urlSendMail, data);
  }
}
