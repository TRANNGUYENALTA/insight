import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ConfigIpService} from '../configIP/config-ip.service';
import {User} from '../../models/setting/user';

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  public ip: any = '';
  private urlGetUsers = '/api/location';
  private urlDeleteUser = '/employee/delete/';
  private urlAddUser = '/api/location/store';
  private urlGetUserById = '/employee/get_id/';
  private urlUpdateUser = '/employee/';


  constructor(private http: HttpClient, private configIp: ConfigIpService) {
    this.ip = configIp.getIpInSight();
    this.urlGetUsers = this.ip + this.urlGetUsers;
    this.urlDeleteUser = this.ip + this.urlDeleteUser;
    this.urlAddUser = this.ip + this.urlAddUser;
    this.urlGetUserById = this.ip + this.urlGetUserById;
    this.urlUpdateUser = this.ip + this.urlUpdateUser;
  }
  getLocation(): Observable<any> {
    return this.http.get<any>(this.urlGetUsers);
  }

  getUSerSettingById(id): Observable<any> {
    const url = this.urlGetUserById + id;
    return this.http.get(url);
  }

  deleteUserSetting(id): Observable<any> {
    const url = this.urlDeleteUser + id;
    return this.http.delete<any>(url);
  }

  addUserSetting(data): Observable<any> {
    return this.http.post<any>(this.urlAddUser, data);
  }

  updateUserSetting(data: User): Observable<any> {
    const url = this.urlUpdateUser + data.id;
    return this.http.put<any>(url, {
      'full_name': data.full_name,
      'phone': data.phone,
      'email': data.email,
      'password': data.password,
      'permission': data.permission
    });
  }

}
