import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ConfigIpService} from '../configIP/config-ip.service';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class FeedsService {
  private ip: any = '';
  private ipInsight: any = '';
  private urlGetAllFeed = '/api/feed_data/anyData';
  private urlGetFeedMonth = '/api/feed_data/get_month';
  private urlAddFeed = '/api/feed_data';
  private urlGetFeedById = '/api/feed_data/show/';
  private urlUpdateFeedById = '/api/feed_data/';

  // Add promo
  private urlGetAllTemplate = '/api/template_example';
  private urlGetTemplateDemo = '/api/template_example_detail/show/';
  private urlGetIconTemplate = '/api/template_example/icon/show/';
  // Get image demo
  private urlGetImageDemo = '/api/template_example/detail/show/';
  private urlAddPromo = '/api/template_detail';
  private urlUpdatePromo = '/api/template_detail/update';
  private urlLaunchData = '/api/template/feed-data/store';
  private urlUpdateLaunchData = '/api/template/feed-data/update';
  private urlSendActiveTemplate = '/api/template/send';
  private urlGetTemplateById = '/api/template_detail/show/';
  // template 03
  private urlGetLogoTemplate03 = '/api/template_example/logo/show/3/1';
  // get gradient for template
  private getGradientImage = '/api/template_detail/get_setting';


  constructor(private http: HttpClient, private configIp: ConfigIpService) {
    this.ip = configIp.getIp();
    this.ipInsight = configIp.getIpInSight();
    // insight
    // api for promo
    this.urlGetAllTemplate = this.ipInsight + this.urlGetAllTemplate;
    this.urlGetTemplateDemo = this.ipInsight + this.urlGetTemplateDemo;
    this.getGradientImage = this.ipInsight + this.getGradientImage;
    this.urlAddPromo = this.ipInsight + this.urlAddPromo;
    this.urlGetTemplateById = this.ipInsight + this.urlGetTemplateById;

    this.urlGetAllFeed = this.ipInsight + this.urlGetAllFeed;
    this.urlGetFeedMonth = this.ipInsight + this.urlGetFeedMonth;
    this.urlAddFeed = this.ipInsight + this.urlAddFeed;
    this.urlUpdatePromo = this.ipInsight + this.urlUpdatePromo;
    this.urlGetFeedById = this.ipInsight + this.urlGetFeedById;
    this.urlUpdateFeedById = this.ipInsight + this.urlUpdateFeedById;
    this.urlGetImageDemo = this.ip + this.urlGetImageDemo;
    this.urlGetIconTemplate = this.ip + this.urlGetIconTemplate;
    this.urlLaunchData = this.ip + this.urlLaunchData;
    this.urlUpdateLaunchData = this.ip + this.urlUpdateLaunchData;
    this.urlSendActiveTemplate = this.ip + this.urlSendActiveTemplate;
    this.urlGetLogoTemplate03 = this.ip + this.urlGetLogoTemplate03;

  }

  // add feed promo
  getAllTemplate(): Observable<any> {
    return this.http.get<any>(this.urlGetAllTemplate);
  }

  getTemplateDemo(id): Observable<any> {
    const url = this.urlGetTemplateDemo + id;
    return this.http.get<any>(url);
  }

  // get Gradient image
  getGradientImageFunction(): Observable<any> {
    const url = this.getGradientImage;
    return this.http.get<any>(url);
  }

// get image demo
  getImageDemo(template, type): Observable<any> {
    const url = this.urlGetImageDemo + template + '/' + type;
    return this.http.get<any>(url);
  }

  // add promo
  addPromo(data): Observable<any> {
    return this.http.post<any>(this.urlAddPromo, data);
  }

  // add promo
  updateProm(data): Observable<any> {
    return this.http.post<any>(this.urlUpdatePromo, data);
  }

  // send active template promo
  sendActiveTemplatePromo(data): Observable<any> {
    return this.http.post<any>(this.urlSendActiveTemplate, data);
  }

  // get template by id
  getTemplateById(id): Observable<any> {
    const url = this.urlGetTemplateById + id;
    return this.http.get<any>(url);
  }

// End promo service
  getAllFeed(_id): Observable<any> {
    const url = this.urlGetAllFeed + '/' + _id;
    return this.http.get<any>(url);

  }

  getFeedsWithMonth(_id): Observable<any> {
    const url = this.urlGetFeedMonth + '/' + _id;
    return this.http.get<any>(url);

  }

  addFeed(feed: FormData): Observable<any> {
    return this.http.post<any>(this.urlAddFeed, feed);
  }

  deleteEvent(id): Observable<any> {
    const url = `${this.urlAddFeed}/${id}`;
    return this.http.delete(url);
  }

// Get Feed By Id
  getFeedById(id): Observable<any> {
    const url = this.urlGetFeedById + id;
    console.log(url);
    return this.http.get<any>(url);
  }

// Update Feed
  updateFeed(id, data): Observable<any> {
    const url = this.urlUpdateFeedById + id;
    return this.http.post<any>(url, data);
  }
}
